# Stride TDD Application #
Included in this directory is a module containing some simple classes and accompanying specs that outline my work history, experience, and what I have to offer as a Full Stack Agile Developer. Stride's approach to software consulting is incredibly exciting to me, and I'd love to chat some more about what you're looking to add to the team and if my background might be a good fit.

## Instructions ##
```bash
$ git clone https://trevornelson@bitbucket.org/trevornelson/stride-application.git
$ cd stride-application
$ bundle install
$ rspec spec --format documentation # passing specs amount to a cover letter of sorts.
$ irb -r ./resume.rb
$ puts Resume::stride_application.to_s # prints a simplified version of my resume to console.
```

### Specs ###
![Screenshot 2016-10-21 18.30.50.png](https://bitbucket.org/repo/Kk9nxG/images/758383136-Screenshot%202016-10-21%2018.30.50.png)

### Resume ###
![Screenshot 2016-10-21 18.30.35.png](https://bitbucket.org/repo/Kk9nxG/images/2652934672-Screenshot%202016-10-21%2018.30.35.png)


## Some Notes ##

- The Resume classes aren't very reusable, and are really specific to this specific instance. I did this, though, because I wanted to tell a story with the specs and classes.
- The specs are also really cumulative, when they ideally should be de-coupled and independent from each other as much as possible. I did this to build the classes and specs with the progression of my background in mind, and try to show how my diverse background could come together to make me an effective software consultant.
- Overriding the Student#learn method is admittedly weird and a code-smell, but I thought it'd be a fun way to communicate that Dev Bootcamp taught me how to learn at a faster pace.
- The classes and specs aren't as DRY as I'd normally aim for, but this was again because I wanted them to be more "narrative" in nature.
- The describe block's description should generally be the class name instead of a "name" for a specific instance, though this makes the specs read better in this case.