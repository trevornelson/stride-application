FactoryGirl.define do
  factory :person, :class => Resume::Person do
    name 'Trevor Nelson'
    phone '3477018258'
    email 'trevor.nelson1@gmail.com'
    linkedin 'trevorandrewnelson'
    github 'trevornelson'
    interests ['Creativity', 'Learning']
  end
end