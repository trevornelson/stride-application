require_relative 'spec_helper'

describe "Trevor Nelson" do

  before :all do
    @trevor_nelson = build(:person)
  end

  it "Exists (hopefully?)" do
    expect(@trevor_nelson).to be_truthy
  end

  it "Email: trevor.nelson1@gmail.com" do
    expect(@trevor_nelson.email).to eq('trevor.nelson1@gmail.com')
  end

  it "Phone: (347) 701-8258" do
    expect(@trevor_nelson.phone).to eq('(347) 701-8258')
  end

  it "LinkedIn: https://www.linkedin.com/in/trevorandrewnelson" do
    expect(@trevor_nelson.linkedin).to eq('https://www.linkedin.com/in/trevorandrewnelson')
  end

  it "GitHub: http://www.github.com/trevornelson" do
    expect(@trevor_nelson.github).to eq('http://www.github.com/trevornelson')
  end

  it "Is passionate about creativity and learning" do
    expect(@trevor_nelson.interests).to include("Creativity", "Learning")
  end

  context "As an University of Oklahoma Student-" do

    before :all do
      @trevor_nelson = @trevor_nelson.attend_college
    end

    it "Obtained broad business management knowledge" do
      expect(@trevor_nelson.skills).to include("Broad Business Knowledge")
    end

    it "Developed keen industry and market analysis skills" do
      expect(@trevor_nelson.skills).to include("Industry Analysis", "Market Analysis")
    end

    it "Established empathy for customers and the ability to gain insight into market needs" do
      expect(@trevor_nelson.skills).to include("Market Insight")
    end

    it "Learned marketing strategy" do
      expect(@trevor_nelson.skills).to include("Marketing Strategy")
    end

  end

  context "As a Digital Strategy Consultant-" do

    before :all do
      @trevor_nelson = @trevor_nelson.digital_strategy_consulting
    end

    it "Demonstrated excellent client-facing consulting skills" do
      expect(@trevor_nelson.skills).to include("Client Engagement Management", "Consulting Project Planning")
    end

    it "Gained experience in web product strategy" do
      expect(@trevor_nelson.skills).to include("Information Architecture", "Conversion Rate Optimization", "User Experience")
    end

    it "Attained fundamental programming skills and a passion for it" do
      expect(@trevor_nelson.skills).to include("Software Development", "Visual Basic")
      expect(@trevor_nelson.interests).to include("Programming")
    end

    it "Strategized and debugged client analytics implementations" do
      expect(@trevor_nelson.skills).to include("Analytics Implementation")
    end

    it "Planned and managed multi-channel digital marketing campaigns" do
      expect(@trevor_nelson.skills).to include("SEO", "Paid Search", "Paid Social", "Display Advertising", "Remarketing")
    end

  end

  context "As a Dev Bootcamp Student-" do

    before :all do
      @trevor_nelson = @trevor_nelson.attend_dev_bootcamp
    end

    it "Developed the ability to learn new technologies very quickly" do
      skills = @trevor_nelson.skills
      expect{ @trevor_nelson.learn("tech 1", "tech 2") }.not_to raise_error
      @trevor_nelson.skills = skills
    end

    it "Practiced Test Driven Development" do
      expect(@trevor_nelson.skills).to include(
        "Test Driven Development",
        "Rspec",
        "Jasmine",
        "Capybara"
      )
    end

    it "Worked effectively as a part of a development team" do
      expect(@trevor_nelson.skills).to include(
        "Agile",
        "Git",
        "Pair Programming",
        "Code Review"
      )
    end

    it "Learned software engineering fundamentals and best practices" do
      expect(@trevor_nelson.skills).to include(
        "Object Oriented Design",
        "Algorithms",
        "Data Structures",
        "Ruby",
        "Javascript",
        "SQL",
        "Unix Command Line"
      )
      expect(@trevor_nelson.interests).to include("Well-Designed Code")
    end

    it "Taught me full stack web development" do
      expect(@trevor_nelson.skills).to include(
        "Sinatra",
        "Ruby on Rails",
        "HTML5",
        "CSS3",
        "jQuery",
        "AngularJS",
        "PostgreSQL"
      )
    end

  end

  context "As a Software Engineer-" do

    before :all do
      @trevor_nelson = @trevor_nelson.aol_software_engineer
    end

    it "Developed my frontend skills" do
      expect(@trevor_nelson.skills).to include(
        "React.js",
        "Backbone.js",
        "Flux",
        "SASS",
        "Alt.js",
        "Webpack"
      )
    end

    it "Developed my backend skills" do
      expect(@trevor_nelson.skills).to include(
        "PHP",
        "Eloquent",
        "Slim Framework",
        "WordPress"
      )
    end

    it "Learned dev ops and deployment skills" do
      expect(@trevor_nelson.skills).to include(
        "AWS",
        "Jenkins",
        "Chef",
        "Grunt"
      )
    end

    it "Gained product management experience" do
      expect(@trevor_nelson.skills).to include(
        "Product Roadmap Planning",
        "Project Management",
        "Q.A.",
        "Codeception Testing Framework",
        "Nightwatch.js"
      )
    end

    context "As a Stride Applicant-" do

      it "Can advise clients on extreme programming" do
        expect { @trevor_nelson.consult_clients_on_extreme_programming }.not_to raise_error
      end

      it "Can code in at least two of Stride's preferred languages" do
        expect { @trevor_nelson.program_in_stride_preferred_languages }.not_to raise_error
      end

      it "Is passionate about learning and am able to learn quickly" do
        expect(@trevor_nelson.interests).to include("Learning")
        expect { @trevor_nelson.learn("Elixir", "Java", "Scala", "Clojure", "Ember", "Django", "Phoenix") }.not_to raise_error
      end

      it "Can help ideate in client MVP projects" do
        expect { @trevor_nelson.develop_product_strategy }.not_to raise_error
      end

    end

  end

end