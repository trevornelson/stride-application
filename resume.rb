module Resume

  XP_SKILLS = ["Test Driven Development", "Pair Programming", "Client Engagement Management"]
  PREFERRED_LANGUAGES = ["Elixir", "Javascript", "Python", "Java", "Scala", "Clojure", "React.js", "Angular.js", "Ember", "Ruby on Rails", "Django", "Phoenix"]
  MVP_SKILLS = ["Product Roadmap Planning", "Broad Business Knowledge", "Market Analysis", "Industry Analysis"]

  def self.stride_application
    require 'factory_girl'
    require_relative 'spec/factories.rb'

    trevor_nelson = FactoryGirl.build(:person)
    trevor_nelson.attend_college
    trevor_nelson.digital_strategy_consulting
    trevor_nelson.attend_dev_bootcamp
    trevor_nelson.aol_software_engineer
  end

  class Person
    attr_accessor :name, :email, :phone, :linkedin, :github, :skills, :interests

    def initialize(params = {})
      @name = params.fetch('name', nil)
      @phone = params.fetch('phone', nil)
      @email = params.fetch('email', nil)
      @linkedin = params.fetch('linkedin', nil)
      @github = params.fetch('github', nil)
      @skills = params.fetch('skills', [])
      @interests = params.fetch('interests', [])
    end

    def phone
      if @phone =~ /^(\d{3})(\d{3})(\d{4})$/
        return "(#{$1}) #{$2}-#{$3}"
      end
    end

    def linkedin
      if @linkedin
        return "https://www.linkedin.com/in/#{@linkedin}"
      end
    end

    def github
      if @github
        return "http://www.github.com/#{@github}"
      end
    end

    def attend_college
      student = Resume::Student.new(self.to_hash)
      student.get_entrepreneurship_degree
      student.get_marketing_degree
      return student.add_stride_skills
    end

    def attend_dev_bootcamp
      student = Resume::Student.new(self.to_hash)
      student.complete_dev_bootcamp
      return student.add_stride_skills
    end

    def digital_strategy_consulting
      consultant = Resume::Consultant.new(self.to_hash)
      consultant.senior_associate
      consultant.manager
      return consultant.add_stride_skills
    end

    def aol_software_engineer
      engineer = Resume::Engineer.new(self.to_hash)
      engineer.stylemepretty_experience
      return engineer.add_stride_skills
    end

    def add_stride_skills
      if (@skills & Resume::XP_SKILLS).any?
        class << self
          def consult_clients_on_extreme_programming
            return @skills & Resume::XP_SKILLS
          end
        end
      end

      if (@skills & Resume::PREFERRED_LANGUAGES).length >= 2
        class << self
          def program_in_stride_preferred_languages
            return @skills & Resume::PREFERRED_LANGUAGES
          end
        end
      end

      if (@skills & Resume::MVP_SKILLS).any?
        class << self
          def develop_product_strategy
            return @skills & Resume::MVP_SKILLS
          end
        end
      end

      return self
    end

    def to_hash
      hash = {}
      instance_variables.each { |atr| hash[atr.to_s.delete("@")] = self.instance_variable_get(atr) }
      return hash
    end

    def to_s
      return to_hash.map { |k, v| "#{k.capitalize}: #{(v.is_a? Array) ? v.sort.join(', ') : v}" }.join("\n\n")
    end

  end


  class Student < Person

    def get_entrepreneurship_degree
      learn("Broad Business Knowledge")
      learn("Industry Analysis")
      learn("Market Analysis")
    end

    def get_marketing_degree
      learn("Market Insight")
      learn("Marketing Strategy")
    end

    def complete_dev_bootcamp
      improve_tech_learning_speed
      development_team_experience
      engineering_fundamentals
      web_app_development
    end

    def development_team_experience
      learn("Agile", "Test Driven Development", "Rspec", "Jasmine", "Capybara", "Git", "Pair Programming", "Code Review")
    end

    def engineering_fundamentals
      learn("Object Oriented Design", "Algorithms", "Data Structures", "Ruby", "Javascript", "SQL", "Unix Command Line")
      @interests << "Well-Designed Code"
    end

    def web_app_development
      learn("Sinatra", "Ruby on Rails", "HTML5", "CSS3", "jQuery", "AngularJS", "PostgreSQL")
    end

    def learn(skill)
      @skills << skill
    end

    def improve_tech_learning_speed
      self.class.class_eval do
        def learn(*args)
          args.each { |skill| @skills << skill }
        end
      end
    end

  end


  class Consultant < Student

    def senior_associate
      execute_digital_marketing_campaigns
      perform_analytics_audits
      build_reporting_platform
    end

    def manager
      manage_client_engagements
      web_product_strategy_consultation
    end

    private
      def execute_digital_marketing_campaigns
        learn("SEO")
        learn("Paid Search")
        learn("Paid Social")
        learn("Display Advertising")
        learn("Remarketing")
      end

      def perform_analytics_audits
        learn("Analytics Implementation")
      end

      def build_reporting_platform
        learn("Software Development")
        learn("Visual Basic")
        @interests << "Programming"
      end

      def manage_client_engagements
        learn("Client Engagement Management")
        learn("Consulting Project Planning")
      end

      def web_product_strategy_consultation
        learn("Information Architecture")
        learn("Conversion Rate Optimization")
        learn("User Experience")
      end

  end


  class Engineer < Consultant

    def stylemepretty_experience
      vault_search_project
      vendor_portfolios_project
      vendor_guide_project
      misc_debugging_and_features
    end

    def vault_search_project
      learn("React.js", "SASS")
    end

    def vendor_portfolios_project
      learn("Flux", "Alt.js", "Eloquent", "Slim Framework", "Codeception Testing Framework", "Nightwatch.js")
    end

    def vendor_guide_project
      learn("Product Roadmap Planning", "Project Management", "Q.A.")
    end

    def misc_debugging_and_features
      learn("AWS", "Jenkins", "Chef", "Grunt", "PHP", "Backbone.js", "WordPress", "Webpack")
    end

  end

end